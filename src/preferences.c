/*

Filename: preferences.c
Copyright © 2010 - 2020  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs

This file is part of Filemanager-AVS.

*/

#include "preferences.h"

struct _FavsPreferences
{
  GtkDialog parent;

  GSettings *settings;

  GtkWidget *okbutton;
  GtkWidget *cancelbutton;
};

G_DEFINE_TYPE (FavsPreferences, favs_preferences, GTK_TYPE_DIALOG)


static void favs_preferences_init (FavsPreferences *win)
{
  gtk_widget_init_template (GTK_WIDGET (win));
}

static void favs_preferences_dispose (GObject *object)
{
  FavsPreferences *win;

  win = FAVS_PREFERENCES (object);

  G_OBJECT_CLASS (favs_preferences_parent_class)->dispose (object);
}

static void on_okbutton_clicked (GtkButton *button, FavsPreferences *win)
{
  gtk_window_close (GTK_WINDOW (win));
}

static void on_cancelbutton_clicked (GtkButton *button, FavsPreferences *win)
{
  gtk_window_close (GTK_WINDOW (win));
}

static void favs_preferences_class_init (FavsPreferencesClass *class)
{
  G_OBJECT_CLASS (class)->dispose = favs_preferences_dispose;
  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class), "/de/acwn/filemanager-avs/preferences.ui");

  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), FavsPreferences, okbutton);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), FavsPreferences, cancelbutton);

  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_okbutton_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_cancelbutton_clicked);
}

FavsPreferences *favs_preferences_new (FavsWindow *win)
{
  return g_object_new (FAVS_PREFERENCES_TYPE, "transient-for", win, NULL);
}
