/*

Filename: helper.h
Copyright © 2010 - 2020  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs

This file is part of Filemanager-AVS.

*/

#ifndef HELPER_H
#define HELPER_H

#include <gtk/gtk.h>

const gchar *get_full_data_path (const gchar *filename);

const gchar *scan_location (const gchar *filename);

void show_message_dialog (const gchar *message01, const gchar *message02);

#endif
