/*

Filename: window.c
Copyright © 2010 - 2020  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs

This file is part of Filemanager-AVS.

*/

#include <glib/gstdio.h>
#include <sys/stat.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "helper.h"
#include "preferences.h"
#include "window.h"


struct _FavsWindow
{
  GtkApplicationWindow parent;

  GtkWidget *scandropdownbutton;
  GtkWidget *preferencesbutton;
  GtkWidget *helpbutton;
  GtkWidget *aboutbutton;
  GtkWidget *locationlabel;
  GtkTreeView *treeview;
  GtkListStore *liststore;
};

G_DEFINE_TYPE (FavsWindow, favs_window, GTK_TYPE_APPLICATION_WINDOW)

static void favs_window_init (FavsWindow *win)
{
  gtk_widget_init_template (GTK_WIDGET (win));
}

static void on_aboutbutton_clicked (FavsWindow *win)
{
  GdkPixbuf *pixbuf = NULL;
  GError *error = NULL;

  pixbuf = gdk_pixbuf_new_from_file (get_full_data_path ("Filemanager-AVS.png"), &error);
  if (pixbuf == NULL)
  {
    show_message_dialog ("Can't open file", error->message);
  }

  gtk_show_about_dialog (GTK_WINDOW (NULL),
  "program-name", PACKAGE_NAME,
  "website"     , "https://www.acwn.de/projects/filemanager-avs/",
  "version"     , PACKAGE_VERSION,
  "license-type", GTK_LICENSE_GPL_3_0,
  "logo", pixbuf,
  NULL);
  g_object_unref (pixbuf);
}

static void on_helpbutton_clicked (GtkToolButton *button, FavsWindow *win)
{
  GError *error = NULL;

  if (!gtk_show_uri_on_window (GTK_WINDOW (win), "https://www.acwn.de/projects/filemanager-avs/help", gtk_get_current_event_time (), &error))
  {
    show_message_dialog ("Can't open link to 'https://www.acwn.de/projects/filemanager-avs/help'. Please do it yourself.", error->message);
  }
}

static void on_preferencesbutton_clicked (GtkToolButton *button)
{
  FavsWindow *win;
  FavsPreferences *preferences;

  win = FAVS_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (button)));
  preferences = favs_preferences_new ( FAVS_WINDOW(win));
  gtk_window_present (GTK_WINDOW (preferences));
}

static void read_location (const gchar *location, FavsWindow *win)
{
  GtkTreeIter iter;
  GDir *directory;
  GError *error = NULL;
  const gchar *recobject;
  struct stat status;

  gtk_list_store_clear (win->liststore);
  directory = g_dir_open (location, 0, &error);
  if (directory != NULL) 
  {
    while ((recobject = g_dir_read_name (directory)))
		{
		  recobject = g_build_filename (location, recobject, NULL);
		  if (g_lstat (recobject, &status) == 0)
			{
			  if (S_ISDIR (status.st_mode))
				{
					read_location (recobject, win);
				}
				else if (S_ISREG (status.st_mode))
				{
					gtk_list_store_append (win->liststore, &iter);
					gtk_list_store_set (win->liststore, &iter, 0, recobject, 1, "Checking", -1);
					gtk_list_store_set (win->liststore, &iter, 0, recobject, 1, scan_location (recobject), -1);
					while(gtk_events_pending()) gtk_main_iteration();
				}
			}
		}
		g_dir_close (directory);
		return;
  }
  else
  {
		gtk_list_store_append (win->liststore, &iter);
		gtk_list_store_set (win->liststore, &iter, 0, location, 1, "Checking", -1);
		gtk_list_store_set (win->liststore, &iter, 0, location, 1, scan_location (location), -1);
  }
}

static void read_list (FavsWindow *win)
{
  GtkTreeIter iter;
  gboolean valid;
  gchar *location;

  valid = gtk_tree_model_get_iter_first (GTK_TREE_MODEL (win->liststore), &iter);
  while (valid)
  {
    gtk_tree_model_get (GTK_TREE_MODEL (win->liststore), &iter, 0, &location, -1);
    gtk_list_store_set (win->liststore, &iter, 0, location, 1, scan_location (location), -1);
    valid = gtk_tree_model_iter_next (GTK_TREE_MODEL (win->liststore), &iter);
  }
}

static void on_scandropdownbutton_clicked (GtkWidget *widget, FavsWindow *win)
{
  const gchar *location = gtk_label_get_text (GTK_LABEL (win->locationlabel));

  if (strlen (location) != 0)
  {
    read_location (gtk_label_get_text (GTK_LABEL(win->locationlabel)), win);
  }
  else
  {
    read_list (win);
  }
}

static void on_selectfile_activate (GtkWidget *widget, FavsWindow *win)
{
  GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
  GtkWidget *dialog;
  gint retval;

  dialog = gtk_file_chooser_dialog_new ("Open File", NULL, action, "Cancel",
           GTK_RESPONSE_CANCEL, "Open", GTK_RESPONSE_ACCEPT, NULL);
  retval = gtk_dialog_run (GTK_DIALOG (dialog));
  if (retval == GTK_RESPONSE_ACCEPT)
  {
    char *filename;
    GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
    filename = gtk_file_chooser_get_filename (chooser);
    gtk_label_set_label (GTK_LABEL (win->locationlabel), filename);
    g_free (filename);
  }
  gtk_widget_destroy (dialog);
}

static void on_selectfolder_activate (GtkWidget *widget, FavsWindow *win)
{
  GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER;
  GtkWidget *dialog;
  gint retval;

  dialog = gtk_file_chooser_dialog_new ("Open Folder", NULL, action, "Cancel",
           GTK_RESPONSE_CANCEL, "Open", GTK_RESPONSE_ACCEPT, NULL);
  retval = gtk_dialog_run (GTK_DIALOG (dialog));
  if (retval == GTK_RESPONSE_ACCEPT)
  {
    char *foldername;
    GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
    foldername = gtk_file_chooser_get_filename (chooser);
    gtk_label_set_label (GTK_LABEL (win->locationlabel), foldername);
    g_free (foldername);
  }
  gtk_widget_destroy (dialog);
}

static void favs_window_dispose (GObject *object)
{
  FavsWindow *win;

  win = FAVS_WINDOW (object);

  G_OBJECT_CLASS (favs_window_parent_class)->dispose (object);
}

static void favs_window_class_init (FavsWindowClass *class)
{
  G_OBJECT_CLASS (class)->dispose = favs_window_dispose;
  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class), "/de/acwn/filemanager-avs/window.ui");

  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), FavsWindow, scandropdownbutton);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), FavsWindow, preferencesbutton);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), FavsWindow, helpbutton);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), FavsWindow, aboutbutton);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), FavsWindow, locationlabel);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), FavsWindow, treeview);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), FavsWindow, liststore);

  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_aboutbutton_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_helpbutton_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_preferencesbutton_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_scandropdownbutton_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_selectfile_activate);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_selectfolder_activate);
}

FavsWindow *favs_window_new (FavsApp *app)
{
  return g_object_new (FAVS_WINDOW_TYPE, "application", app, NULL); 
}

void favs_window_open (FavsWindow *win, GFile *file)
{
  GtkTreeIter iter;
  const gchar *location = g_file_get_path (file);

  if (g_file_test (location, G_FILE_TEST_IS_DIR))
  {
    gtk_label_set_label (GTK_LABEL (win->locationlabel), location);
  }
  if (g_file_test (location, G_FILE_TEST_IS_REGULAR))
  {
    gtk_list_store_append (win->liststore, &iter);
    gtk_list_store_set (win->liststore, &iter, 0, location, 1, "Ready for scanning ...", -1);
    gtk_label_set_label (GTK_LABEL (win->locationlabel), NULL);
  }
}
